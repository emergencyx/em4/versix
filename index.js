// Load .env configuration to process.env
require('dotenv').config();

const requests = require('./src/requests');
const http = require('http');

const server = http.createServer((request, response) => {
  requests.handle(request, response);
});

server.listen(process.env.APP_PORT, process.env.APP_HOST);
console.log('server up', process.env.APP_HOST, process.env.APP_PORT);