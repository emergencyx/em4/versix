const url = require('url');
const OK_HEAD = 'ACK:HTTP/1.1 200 OK.';
const OK_STATUS = "🚀";

async function handle(request, response) {
    const query = url.parse(request.url, true);
    if (query.pathname === '/status') {
        response.setHeader("Content-Type", "text/plain; charset=utf-8")
        response.end(OK_STATUS);
        return;
    }
    
    if (query.pathname === '/verSix.php') {
        if (request.method === 'HEAD') {
            request.socket.end(OK_HEAD);
            return;
        }

        const message = await execute(request, response, query.query);
        if (message) {
            request.socket.end(message);
            return;
        }
    }

    response.statusCode = 500;
    response.end();
}

async function execute(request, response, query) {
    switch (query.cmd) {
        case 'matchmakingserver': {
            if (query.product !== 'emergency4') {
                return "NACK:unrecognized command";
            }

            return 'ACK:' + process.env.MATCHMAKING_SERVER;
        }

        case 'versixlist': {
            return 'ACK:'; // ???
        }

        case 'recentversion': {
            return 'ACK:' + process.env.RECENT_VERSION;
        }

        case 'recentredirect': {
            return 'ACK:' + process.env.RECENT_REDIRECT;
        }

        default: {
            return 'NACK:unrecognized command';
        }
    }
}

module.exports = { handle };